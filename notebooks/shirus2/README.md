# ECE 445 Lab Notebook: Shiru Shong 

## Entries
[08/27/2021: Team Finding](#08272021-team-finding) \
[09/02/2021: First Team Meeting and RFA Completion](#09022021-first-team-meeting-and-rfa-completion) \
[09/04/2021: First Meeting With Sponsors](#09042021-first-meeting-with-sponsors) \
[09/14/2021: First TA Meeting](#09142021-first-ta-meeting) \
[09/16/2021: Components Research and Project Proposal](#09162021-components-research-and-project-proposal) \
[09/20/2021: Research on Microcontroller and Design Document Check](#09202021-research-on-microcontroller-and-design-document-check) \
[09/25/2021: Design Document and PCB Brainstorming](#09252021-design-document-and-pcb-brainstorming) \
[10/06/2021: New EpiCap Design and Prepare for Design Review](#10062021-new-epicap-design-and-prepare-for-design-review) \
[10/08/2021: Confirmation on SD Card Design](#10082021-confirmation-on-sd-card-design) \
[10/15/2021: Verification LT Spice Simulations and STM32 Schematic](#10152021-verification-lt-spice-simulations-and-stm32-schematic) \
[10/20/2021: Working on Schematic and PCB](#10202021-working-on-schematic-and-pcb) \
[10/30/2021: Finalizing PCB and Update Component List](#10302021-finalizing-pcb-and-update-component-list) \
[11/03/2021: Unit Testing Components and Raspberry Pi Zero](#11032021-unit-testing-components-and-raspberry-pi-zero) \
[11/05/2021: OpenBCI Testing](#11052021-openbci-testing) \
[11/07/2021: PCB Soldering and Testing](#11072021-pcb-soldering-and-testing) \
[11/17/2021: Flash to STM32 and Ensure that Hardware Works for Mock Demo](#11172021-flash-to-stm32-and-ensure-that-hardware-works-for-mock-demo) \
[11/24/2021: Work on the Raspberry Pi Zero Camera Module](#11242021-work-on-the-raspberry-pi-zero-camera-module) \
[11/26/2021: Design 3D PCB Enclosure](#11262021-design-3d-pcb-enclosure) \
[11/29/2021: Work on the integration of ADC and Camera Module for Raspberry Pi Zero Software](#11292021-work-on-the-integration-of-adc-and-camera-module-for-raspberry-pi-zero-software) \
[12/01/2021: ADC Biasing Circuit Error and Demo](#12012021-adc-biasing-circuit-error-and-demo)

## 08/27/2021: Team Finding
**Objectives:** Find a team to work with. Work on the final project idea as a group and begin constructing the RFA.

**Outcome:** I reached out to muliple teams that posted their Initial Posts on the Web Board, which discussed their ideas for final projects. I gave multiple feedbacks on ways to improve/modify their current ideas of final project. A few teams responded to my emails and I felt that the sponsored EpiCap project was particularly interesting as it had a real potential for making an impact in the medical field. At the same time, a remote student, Qihang Zhao (Computer Engineering Student), also reached out to me in regards of working together on the Epicap Project with Casey Bryniarski (Electrical Engineering Student). \
Based on our technical backgorund and knowledge, we decided that we would be splitting into a hardware team of Casey and I while Qihang lead the firmware/software components of the project. In the hardware side, we would be building the schematic/PCB board, PCB enclsoure, while making sure the entire hardware subsystem is functional. 

## 09/02/2021: First Team Meeting and RFA Completion
**Objectives:** Learn more and gain a high-level understanding about the EpiCap Project from our spnsor, Kenny's slide deck and design specification documents. Meet with team members first time to discuss expectations and weekly objectives. 

**Outcome:** Kenny has provided us with a lot of important information about high-level requirements for the project. The two most important technical objectives discussed are: 1. There should be a wide angle camera that records patient's eyes during seizure and store on the SD card. 2. The ADC data when converted to EEG data should be recorded on the on-board SD card for physician's use. Not only that, the cap must appear to be unobtrusive and remain portable for the patient's comfortability. We also discussed that a web app would be helpful in terms of presentation of measurement or EEG data. We would want the data from the board to be processed into a report that can be annotated by a physician. I met with Casey to discuss more technical specification about isolation and power subsystem of the board before refining our RFA for project approval. We decided to reference a lot of our design considerations from the [OpenBCI open source project][openbci-link]. Our RFA includes the following as the criterion of success: 

"Our project needs to provide an experience that is familiar to a physician while also being unobtrusive and durable enough for a patient to use without the aid of a tech or a physician. Our design needs to effectively remain in standby and only begin to record or send data when a patient is experiencing an event. Our device should look like a genuine medical product while also not looking like an appliance or burden for the patient, and needs to present itself like a tool, not a burden for IT."

Kenny's feedback towards the web app implementation: Would be great, but if you guys want to work more on the hardware/firmware stuff, can deprioritize this and just use the openbci software (but somehow be able to show camera feed/recording as well).

[openbci-link]: https://docs.openbci.com/Cyton/CytonSpecs/

## 09/04/2021: First Meeting With Sponsors
**Objectives:** Meet with sponsors Kenny Leung and Jennifer Cortes to learn more about the project expectations and technical requirements. Understand the scope of the project better while clarify specific details. 

**Outcome:** We zoomed Kenny Leung and Jennifer Cortes in order to learn more about the specifics of the project such as budget and ways to order our components required for the project through the Carle Health Funding. We learned that the most important aspects of the project are to implement the wide-angle camera and storage of EEG data in the hardware subsystem. We also learned that it is important to keep the electrodes detected on the head at all times to ensure accurate EEG data collection. To ensure this, we decided that we would be using the electrodes given from the OpenBCI cap. Additionally, we would also be buying the OpenBCI cap to learn the hardware specifis of the entire system and validate that our device works the same way as the OpenBCI cap. We would also be using the OpenBCI GUI to display our EEG data and compare the data from the OpenBCI cap. Kenny also sent us a list of equipments/items he has in his apartments such as the 3D printer filament, Raspberry Pi, wide-angle camera,...which all can be used for our project. The zoom call was really helpful as it allowed us to confirm which apsects of the project are important and which are not as essential to implement. Kenny also provided us with the following block diagram with his initial design idea in mind:
![](notebooks/images/image001.png)

## 09/14/2021: First TA Meeting
**Objectives:** Meet with our TA, Josephine and understand the outline/schedule of the EpiCap project. 

**Outcome:** Before the meeting, we drafted most of the proposal in order to show our TA, Josephine Melia, and receive some feedbacks on the things we should modify in our proposal. The most important aspect of the proposal is that the 3 high-level requirements would need to be clear and concise. We also included a visual aid and a block diagram in the draft proposal and received some feedback about the block diagram of our entire system. In our draft proposal, Josephine pointed out that our design requirements were clear for each subsystem required. Josephine also gave us pointers in how we should approach the safety and ethics section of the proposal. Josephine also gave us a lot of feedback in how we should document our entire project (such as notebook and design document). Overall, Josephine said we needed to put more refinement work in our project proposal but the project idea seems to be strong.

![](notebooks/images/visual_aid.PNG) \
![](notebooks/images/first_block_diagram.PNG)

## 09/16/2021: Components Research and Project Proposal
**Objectives:** Decide on the main components required for the project (that meets the high-level requirements). Revise and finish the project proposal. 

**Outcome:** Our team decided that for the microcontroller that we would be using is the STM32 MCU. For this microcontroller, it would be managing the GSM (global system for mobile), camera, flash storage, and also the ADC converter (collecting the EEG data from electrodes). The STM32 will be mainly responsible for communicating and controling other subystems. For the battery, our team decided that we would be using the LiPo battery as it is very portable and can provide a large amount of power for at least 24 hours. The other most important component that we had to choose is the ADC unit for the board. We decided to use the ADS1299 (8-channel) to sample the EEG data. This is because the chip is known to be used in devices that collect EEG data, just like the OpenBCI cap. For the rest of the components, we have chosen on the high level requireemnts needed for each but have yet to find the specific component. We have stated all this in our project proposal in our device specifications. We have also made the changes that Josephine has told us to do a few days ago. 

- [STM32F4 MCU](https://www.st.com/en/microcontrollers-microprocessors/stm32f4-series.html)
- [ADS1299](https://www.ti.com/product/ADS1299)
- [GSM/SIM800](https://nettigo.eu/products/sim800l-gsm-grps-module)

## 09/20/2021: Research on Microcontroller and Design Document Check
**Objectives:** Learn more about the STM32 F4 series. Prepare for Design Document Check, fix the introduction (problem statement and solution) from the project proposal. 

**Outcome:** 

[STM32F4 MCU](https://www.st.com/resource/en/user_manual/dm00105879-description-of-stm32f4-hal-and-ll-drivers-stmicroelectronics.pdf):
- Use STM32CubeMX that allows user to flash C code into the microcontroller and debug
- Has HAL drivers that covers set of functions for the most common peripheral features
- Has HC-05 Bluetooth module
- Supports [ADC peripheral](https://visualgdb.com/tutorials/arm/stm32/adc/) with high sampling rate and high resolution
    - Has different modes: continuous converison, scan, discontinuous 
- Supports wireless connectivity 
- Example: [STM32 Blue Pill Project](https://stm32-base.org/boards/STM32F103C8T6-Blue-Pill.html)

After researching about the STM32F4 MCU chip, we decided that the chip has all the functions and properties that meets our requirements for the microcontroller. In the second part of this session, we were discussing ways to improve on our design document as we received a poor score for our project proposal. I was in charge of changing our introduction and making it more concise while adding references. With that, I was also in charge of creating the RV tables for the power subsystem. For our LiPo Battery: 

Requirements: 
1. Provide  reasonably low noise (<uV).
2. Must be readily rechargeable, and not provide too much weight (<220g) or bulk to the whole device.
3. During discharging at maximum current and voltage, the temperature of the Li-Po battery would be less than 27℃.

Verification:
1. Take oscilloscope measurements to measure the output voltage ripple signal is less than 1 uV. 
2. Discharge the Li-Po battery entirely to 3.2V and charge the battery to 4.2V in order to ensure the battery cell voltage drops between 3.5-3.7V.
Weigh the battery using a weighing scale to ensure the battery is less than 220g. 
3. Charge the Li-Po battery entirely to 4.2V to ensure that the cell voltage is at its highest.
Discharge the battery and  use an IR thermometer to ensure that the battery does not discharge at a temperature greater than 27℃.

## 09/25/2021: Design Document and PCB Brainstorming
**Objectives:** Assign tasks and discuss the design verifications in the design document. Finalize the microcontroller that we would like to use. Start PCB design. 

**Outcome:** We decided that we are going to use LTSpice to simulate the design and observe the output ripple for many component's noise requirements. We ran into the problem of STM32F4 chip being out of stock right now. We had to resort to finding another chip that meets most of our specifications, which is STM32F2 MCU series. This chip has a lot less libaries and also doesn't have bluetooth or wireless functions. Therefore, we are not able to implement the GSM800 module with just the STM32F2 chip. We identified that one of the biggest challenges we would face is storing both the camera footage and EEG ata on the onboard SD card using the same bus. We would need to consult a TA in regards of how to do this. We have placed two components on the PCB: ADS1299 and STM32F2 MCU. We have also received our DIY OpenBCI Kit and will be 3D printing our headset for using the OpenBCI device as verification of our EpiCap

For our design verifications in DD, we decided that noise isolation is one of the most important factor to not corrupt our EEG data. With that, the noise requirement should be present in all devices. Therefore, we would need to measure the output voltage ripple when unit testing all of our device components. 

## 10/06/2021: New EpiCap Design and Prepare for Design Review
**Objectives:** Understand new Epicap design as we added a new microcontroller. Prepare for Design Review by understanding technical terms of regulators, different types of communications, etc.

**Outcome:** After talking to David (TA), we realized that we would need to change the overall scope of the projct as the STM32F2 microcontroller has limited functions as it doesn't allow camera interface. In order to solve this problem, we would be adding the Raspberry Pi Zero microcontroller to our camera module. The Pi Zero has a built-in camera interface and the videos can be stored directly onto the Pi's SD card. The STM32F2 is acts as our main microcontroller that collects ADC data from the electrodes and detects when to trigger the Raspberry Pi Zero to start recording and also starts storing the ADC data onto the onboard SD card. Therefore while the core of the board remains the same: sensors -> ADC -> STM32 -> SD card storage, the camera interface would include the additional Pi Zero. We also decided that the GSM module should be disregarded from our project as it is not directly applicable to our core requirements and the STM32 microcontroller does not support it. With these changes, we also realized that we would need an indicator LEDs in order to allow user to know what mode the device is in, when the sensor is detached, or when the board is almost out of battery. 

1. Regulator 
linear regulators vs switching regulators\
Linear regulators: powering low powered devices, difference between input and output is small.Net efficient. Small and cheap. Noise: low. Requires a minimum amount higher than output.\
Switching regulators:  Highly efficient, buck-boose converters. Noise: medium-high. Range Vin is high.

2. LDO
DC regulator that can regulate output voltage even when supply voltage is close to output. \
noise -> efficiency -> cause noise can corrupt our EEG data \
Output higher than input -> charge pump \
Output lower -> linear regulator 

UART: host communicates with auxilary device. Bi-directional sending and receiving bits asychronous, no clock. \
SPI: different form of serial communicaiton but specially designed for microcontroller. \
RS232: Specifies voltage levels. RS-232 more rsistant to interference. Defining signals between two devices \
5V-TTL: TTL, circuit built from bipolar transistors for logic states. \
Ethernet: Uses transport and control protocals including TCP, IP, and media access.Wired computer networking technologies. \
I2C: Similar to UART but for modules and sensors. -> Need this for the camera microcontroller as it is for a module  and faster connection (image sensor).

## 10/08/2021: Confirmation on SD Card Design
**Objectives:** Do more research on the SD card the microcontroller interface design. Confirm that the design works. 

**Outcome:**
micro SD card and Microcontroller Interface:
- Utilize an SDIO interface between microcontroller and the micro SD card. 
    - SDIO: High speed data I/O functions combined wih the memory capability to the SD card. 
    - Operates at 50 MHz.
    - Can use either 17-bit or 4-bit data trasnfer.
    - SDIO uses different lines for commands and data while being faster than SPI. 
![](notebooks/images/sdio_interface.PNG)

## 10/15/2021: Confirmation on Parts and Components List
**Objectives:** Confirm on all the main device components that we would need to order. Start working on the schematic and design structure for each subsystem. 

**Outcome:** 

Camera (OV5642): 
- Raspberry Pi Zero triggers the camera module to turn on and store the camera footage back to the Raspberry Pi's SD card. 
- Require a camera module of wide-angle camera to track eyes and arm movement above shoulder hight. 

STM32F205VCT7x MCU (3.3V)
- Problem: not enough RAM for camera module so use the Raspberry Pi Zero. 
- Main microcontroller that controls the SPI between all different modules. Communication: UART and SPI between ADC, SD, camera.

Idea: 
- Use 2 different SD cards for storing camera footage from Raspberry Pi and EEG data from ADC. 
- 32 filessytem sotirng format cause need more than 4 GB of storage.
    - Need to test that the data transferred to the microSD is valid from external use.

Question: How to test?
- Possible solution: simple test case to microcontroller to write a dummy file to the micro SD card. 
    - File stored in the microSD = .txt file viewed on another device. 

## 10/15/2021: Verification LT Spice Simulations and STM32 Schematic
**Objectives:** Perform LT Spice simulations for tolerance analysis. Help Casey with the STM32 Schematic

**Outcome:** For tolerance analysis, we wanted to ensure that the connection between the elecrodes and ADC is secure. We needed to also make sure that the signals received by the electrodes is transferred properly to the ADC. For this, we would need to simulate an RC circuit to perform a low-pass filter on all of the signals received from the electrodes. We chose a 2.2k resistor with 1% tolerance and a 1000 pF capacitor with +/- 20% tolerance. With that, we simulated the RC isolation circuit in LT Spice:

![](notebooks/images/lt_spice_circuit.PNG)

From the LT Spice simulation, we obesrved that even in the worst case scneario, our noise will never go above its tolerance level to clamp the signals before our critical 240 Hz frequency. When the RC is at its minimum, the attenuation still doesn't start before 240 Hz. Therefore, we verified that our components will satisfy our design requirements for the ADC. 

![](notebooks/images/attenuation_plot.PNG)

After verifying the LT Spice simulation result, we started to work no the actual schematic for the ADC and STM32. As Casey have been referencing other sources for how to wire up the circuit, I have been making sure that the schematic is constructed properly and ensure the components we chose will meet the design specifications. We referenced a lot of OpenBCI schematic and also the STM32 Blue Pill Development Board schematic. With that, we created the following schematic on the day of:

![](notebooks/images/adc_schematic.PNG)

## 10/20/2021: Working on Schematic and PCB
**Objectives:** Work with Casey to finish up the schematic and PCB so it can be ordered. 

**Outcome:** As our team is falling behind, through the week, I was helping Casey with finishing up on the schematic and PCB. We realized that our schematic has a lot of subsytems that require a lot of detail connections. Our other partner, Qihang is currently working on the firmware of the STM32. With that said, in the session, we finished the STM32 subsystem and also the SD card storage unit. We created a header for the Raspberry Pi on the side of the board for the camera module. Throughout the session, I am helping Casey look up datasheets and reference circuits as he place the components in the circuit (same as previous session). We did run into problems such as isolating the grounds, which caused a lot of problems when routing the PCB and placing vias. We also had a lot of capacitors for isolation and routing those were hard to keep track of. The following screenshots show the things that we worked on: 
- Power Subsystem Schematic
![](notebooks/images/power_circuit.PNG)

- Storage Unit Schematic \
![](notebooks/images/storage_circuit.PNG)

## 10/30/2021: Finalizing PCB and Update Component List
**Objectives:** Finish and Order the PCB. Update Component List

**Outcome:** In this session, our main goal is to finish fabricating and order the PCB. We decided to layout our PCB with isolated grounds in order to reduce noise while also placing vias all over in order to ensure all the connections are made properly. With that, we also created a reset circuit with the NRST pin to ensure that we are able to reset the microcontroller to standby mode in case something goes wrong. We are also able to finish all the connections of the STM32 and update the pinout to CubeMX for firmware. We were finally able to place the order with JLCPCB and PCBWay. This is the final product of our first board:
- STM32 with Reset Circuit \
![](notebooks/images/stm32_with_reset.PNG)

- Final PCB \
![](notebooks/images/pcb_board.PNG)

In the second part of this session, I had to update the component list as I received the finalized Gerber file for the PCB board. I was able to use the Carle and ECE 445 funding for all the components we had to order. I made sure to order twice the amount we need in case we accidentally break any of the components. The table below illustrates the final cost required for all the components of our first PCB board:

![](notebooks/images/cost_table.PNG)

## 11/03/2021: Unit Testing Components and Raspberry Pi Zero
**Objectives:** Unit test components that arrived and start working on Raspberry Pi Zero

**Outcome:** We first realize that we can test the linear regulator and charge pump to ensure that we get the voltage ripple within the specification. We require the output voltage ripple to be low as we need to ensure that the ripple doesn't corrupt our ADC component. With that, we built a test circuit to measure the output voltage ripple. 

- Test Circuit for Linear Regulator \
![](notebooks/images/IMG-6540.jpg)

- Output Voltage Ripple for Linear Regulator \
![](notebooks/images/IMG-6539.jpg)

We observe that the output voltage ripple is only 45 mV, which meets the specification of the ripple being less than 5% from our RV table. With that, we started to work on the Raspberry Pi Zero. We noticed that we would have to SSH to the Raspberry Pi Zero Headless as we do not have the proper display set up. This was a little tricky as we needed to configure the WiFi and the system using the terminal of PUTTY. However, using the following resources, we were able to set up the Raspberry Pi Zero properly: [Rapberry Pi Zero Headless Wifi Setup](https://core-electronics.com.au/tutorials/raspberry-pi-zerow-headless-wifi-setup.html#troubleshooting), [Headless Pi Zero Ssh Access over USB Windows](https://desertbot.io/blog/headless-pi-zero-ssh-access-over-usb-windows). With the two websites, we were able to set up the WiFi connection through modifying the following files in the boot section of the Raspberry Pi Zero SD card. 
wpa_supplicant.conf:

```
country=US
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
network={
	ssid="wifiName"
	psk="wifiPassword"
	key_mgmt=WPA-PSK
}
```
## 11/05/2021: OpenBCI Testing
**Objectives:** Construct the OpenBCI Helmet and test with the OpenBCI GUI

**Outcome:** As our OpenBCI 3D printed parts came, we were able to construct the OpenBCI physical helmet with the electrodes. We were able to receive data from the electrodes and visualize that data with the OpenBCI GUI. We plan to use the OpenBCI helmet as a way to verify that our EpiCap works. You can view the video of our OpenBCI GUI livestream [here](https://drive.google.com/file/d/1ksq6UuqWVtWL55iHntHTiTTD_SqweEI8/view?usp=sharing). 
## 11/07/2021: PCB Soldering and Testing
**Objectives:** Solder all the components on the PCB and test that all the subsystems are working. Ensure that the STM32 has no shorts and the power subsystem's charge pump is properly soldered on the board. 

**Outcome:** As our PCB just arrived, we were working on soldering all the components on the PCB. The STM32 was the hardest one to solder as it has 100 pins all next to each other, we had to ensure that the none of the STM32's pins are shorted so that the PCB can work properly. We ran into a problem when one of the capacitors was shorting the entire circuit. To fix this, we had to take out the capacitor one by one to make sure that we fix the short. Additionally, we realized that our charge pump is not soldered to the board correctly as some of the pins are touching the pads. The charge pump has a really small footprint which makes it really hard to solder. This took multiple attempts but with a TA's help, we were able to solder in the the charge pump with hot air gun. We also realize that when separating the grounds (analog and digital), the power subsytem is behaving not normally. When referencing the application circuit for the [charge pump](https://www.analog.com/media/en/technical-documentation/data-sheets/3204fa.pdf), we realize that the 2 pins must have the same ground connected. With that said, when we use a wire to jump the two pins together, we were able to achieve the 5V output requried from the charge pump. 

## 11/17/2021: Flash to STM32 and Ensure that Hardware Works for Mock Demo
**Objectives:** Try to flash our firmware code to STM32 and start debugging. Ensure that we are able to display the hardware system works for the Mock Demo

**Outcome:** In thie session, we were able to confirm that once everything is wired up and connected to the debug ST Link V2. We were able to use Keil to trace and see that our register values do change in our STM32, which is a really good sign. We had to resolder a lot of connections before this could happen. However, we ran into some firmware problems such as being stuck in an HSE clock loop and unable to go to the actual program of the code. You can view the video of us stepping through our firmware using Keil in the following link: [STM32 Keil Video](https://drive.google.com/file/d/1y1802UfRkbq0pxL9aSwf_A9FN_AhlecL/view?usp=sharing). We were able to show that this works to our TA during Mock Demo. During the mock demo, we were able to discuss our next steps. \
Next steps:
- Qihang will continue to use the trace feauture to debug the problems with the HSE clock.
- Figure out the Raspberry Pi Zero camera connection as we have been having troubles with using the camera when the Raspbian is updated to the newest version.
- Casey will start working on a new board with just the Raspberry Pi Zero as the main microcontroller in the case that our first board with the STM32 doesn't work. 

## 11/24/2021: Work on the Raspberry Pi Zero Camera Module
**Objectives:** Work on the code for Raspberry Pi Zero for the camera to record. Debug the code and ensure the camera module wide-angle is able to capture the eye movement and arm movement above shoulder height. 

**Outcome:** We received a new Raspberry Pi Zero and wide-angle camera module from Kenny as I believe that we broke the hardware camera strip on our old Raspberry Pi Zero. With that, I was able to work on the Raspberry Pi Zero camera module during the entire Thanksgiving break. With Qihang's old code, I was able to modify and test the code so that there would be a video output saved in the date-time format in a folder called Videos. This is a more helpful code as we will be able to use the Raspberry Pi to call another python folder with the Raspberry Pi code instead of waiting for a signal using SPI. Additionally, Qihang's code had a bit streaming section which was not required and output incorrectly when the function is called. I fixed that by just taking the function out and replace it with the camera.wait_recording function. Here is the code for the camera module that is used later when presenting the camera module during demo. 

```
import os
from datetime import datetime
import io
import picamera
import serial
from time import sleep

secs_before = 10
trigger_fileextension = '.trg'
trigger_path = 'trigger/'
video_path = 'Videos/'

# Format of trigger file name:          2021-11-03-08-05-00.trg
# sudo apt-get install python3-serial


with picamera.PiCamera() as camera:
    camera.resolution = (300, 300) #set width and height of the image
    camera.framerate = 25 #how many image per second
    #stream = picamera.PiCameraCircularIO(camera, seconds=(secs_before + 10))#record 20s video
    stream = io.BytesIO() 
   # camera.start_preview()
   # print('Driving Recorder')
   #  try:
   # ser = serial.Serial("/dev/ttyS0", 9600)  # Open port ttyS0 with baud rate:9600Bd
       # ser.open()
       # while True:
            #received_data = ser.read(10)  # read serial port, read 10 characters
    now = datetime.now() #output the current time
    str_time = datetime.strftime(now, '%Y-%m-%d %H:%M:%S') #param1: object, param2: ex(2021-11-3 23:30:42)
    camera.start_recording(video_path+str_time+'.h264') # param1: name of the file, param2: format of the file
    camera.wait_recording(20) #start recording, recording total seconds
   # camera.split_recording(video_path + str_time + '.h264') #split the recording and store it depending on saving time. ex:2021-11-03-08-05-00.h264
    print('New files create')
    camera.stop_recording() #stop recording
   # finally:
   # ser.close() #close port
    #camera.stop_recording() #stop recording
```

You can view a recording of the camera module working by clicking on the following link: [Camera Module Video - Eye Tracking](https://drive.google.com/file/d/1ZYWozGNANGjWmADlKznPD2Os_edaO_GK/view?usp=sharing)

## 11/26/2021: Design 3D PCB Enclosure
**Objectives:** Design the 3D Enclosure for the PCB using Fusion 360.

**Outcome:** I decided to build a 3D enclosure for the PCB board using Fusion 360. I watched multiple tutorials on YouTube for instructions as this is my first time using Fusion 360. It took around 6 hours of building for designing an enclosure that fits our first board with the cover. Here are the screenshots of my 3D PCB enclosure: 

![](notebooks/images/3d_model_1.PNG)
![](notebooks/images/3d_model_2.PNG)

## 11/29/2021: Work on the integration of ADC and Camera Module for Raspberry Pi Zero Software
**Objectives:** Have to work on software for the integration between the ADC and Camera Module. Also help work on the new PCB for the Raspberry Pi and ADC. 

**Outcome:** As our second round of PCB just arrived, we needed to solder the ADC and also the of the components on. Casey was mainly responsible for this while I work on debugging the Raspberry Pi Zero software code for the ADC and Camera Module. We were able to test that when the ADC board is connected to the Raspberry Pi Zero, we were able to communicate with the ADC and receive test data. Qihang was able to write a code with ring buffer in order to detect when a seizure is going to occur, which is going to trigger the ADC to save the data to the Data folder and the camera to start recording and save to the Videos folder. We discovered a problem when we tried to test with the OpenBCI helmet and the data collected from the ADC is still zero. Casey and I was trying to debug this for awhile and we thought that we might have bricked our ADC chip. 

ADC Test Data Code: 

```
from RaspberryPiADS1299 import ADS1299_API
from time import time, sleep

# please fix!!
def DefaultCallback(data):
    pass
    print (repr(data))

# init ads api
ads = ADS1299_API()

# init device
ads.openDevice()
# attach default callback
ads.registerClient(DefaultCallback)
# configure ads
ads.configure(sampling_rate=1000)

print ("ADS1299 API test stream starting")

# begin test streaming
# ads.startTestStream()

# begin EEG streaming
ads.startEegStream()

# wait
sleep(10)

print ("ADS1299 API test stream stopping")

# stop device
ads.stopStream()
# clean up
ads.closeDevice()

sleep(1)
print ("Test Over")
```

ADC with Camera Module Code: 

```
# ads_test.py by Qihang Zhao (qihangz2) Shiru Shong (shirus2) Casey Bryniarski (bryniar2)
# For epicap project
# This script 1. opens ADS SPI lanes for communication
# Takes output stream from ADS SPI and places into buffer
# Compares first half of FIFO buffer with second half of FIFO buffer
# Determines whether seizure activity detected
# When activity is detected, records EEG data and initiates camera recording

from RaspberryPiADS1299 import ADS1299_API
from time import sleep
from datetime import datetime
#import time
import sys
# please fix!!
#sys.path.append("/home/pi")
#import Record_test

import numpy
import os
# create ringbuffer of 10 entries of 8 float data (8 channels, uV measurement readout)
class RingBuffer:
    def __init__(self, size):
      self.data = [None for i in range(size)]

    def append(self, x):
      self.data.pop(0)
      self.data.append(x)

    def get(self):
      return  self.data

    def pop(self):
      self.data.pop(0)
      self.data.append(0)

buf = RingBuffer(80)

prev = 0
cur = 0
flag = 0
def DefaultCallback(data):
    #pass
    print (repr(data))
    global prev
    global cur
    global flag
    global buf
    if flag == 0:
      for i in range(len(data)):
        buf.append(data[i])
        prev = prev + data[i]
      if (buf.get())[0] != None:
        flag = 1
    (buf.get())[0] = 0
    if flag == 1:
      for i in range(len(data)):
        buf.append(data[i])
        cur = cur + data[i]

      if (buf.get())[0] != 0:
        prev = cur
        cur = 0
#1.5 is the threshold , I use ringbuffer to store  80 numbers, since we want 10 set of data, and we have 8 channels, which mean we will get 8 numbers per time
    if (cur/80) >= 1.5*(prev/80):
      os.system('python3 Record_test.py')
#      os.system('ls')
# print stdout to file
file_path = 'Data/'
now = datetime.now()
str_time = datetime.strftime(now,'%Y-%m-%d %H:%M:%S')
sys.stdout = open(file_path+str_time+'.txt', "w")

# init ads api
ads = ADS1299_API()

# init device
ads.openDevice()
# attach default callback
ads.registerClient(DefaultCallback)
# configure ads
ads.configure(sampling_rate=1000)

print ("ADS1299 API test stream starting")

# begin test streaming
#ads.startTestStream()
#T = 0
#T1 = time.time()
# begin EEG streaming
#while T < 10:
ads.startEegStream()
print (ads.startEegStream())
 # T2 = time.time()
 # T = T2-T1
 # i = i - 1
# wait
sleep(15)

print ("ADS1299 API test stream stopping")

# stop device
ads.stopStream()
# clean up
ads.closeDevice()

sleep(1)
print ("Test Over")
```

Data received from the ADC: 

```
ADS1299 API test stream starting
array([-1.21e-08, -1.21e-08, -1.21e-08, -1.21e-08, -1.21e-08, -1.21e-08,
       -1.21e-08, -1.21e-08])
array([-1.21e-08, -1.21e-08, -1.21e-08, -1.21e-08, -1.21e-08, -1.21e-08,
       -1.21e-08, -1.21e-08])
array([-1.21e-08, -1.21e-08, -1.21e-08, -1.21e-08, -1.21e-08, -1.21e-08,
       -1.21e-08, -1.21e-08])
array([-1.21e-08, -1.21e-08, -1.21e-08, -1.21e-08, -1.21e-08, -1.21e-08,
       -1.21e-08, -1.21e-08])
array([-1.21e-08, -1.21e-08, -1.21e-08, -1.21e-08, -1.21e-08, -1.21e-08,
       -1.21e-08, -1.21e-08])
array([-1.21e-08, -1.21e-08, -1.21e-08, -1.21e-08, -1.21e-08, -1.21e-08,
       -1.21e-08, -1.21e-08])
array([-1.21e-08, -1.21e-08, -1.21e-08, -1.21e-08, -1.21e-08, -1.21e-08,
       -1.21e-08, -1.21e-08])
array([-1.21e-08, -1.21e-08, -1.21e-08, -1.21e-08, -1.21e-08, -1.21e-08,
       -1.21e-08, -1.21e-08])
array([-1.21e-08, -1.21e-08, -1.21e-08, -1.21e-08, -1.21e-08, -1.21e-08,
       -1.21e-08, -1.21e-08])
array([-1.21e-08, -1.21e-08, -1.21e-08, -1.21e-08, -1.21e-08, -1.21e-08,
       -1.21e-08, -1.21e-08])
array([-1.21e-08, -1.21e-08, -1.21e-08, -1.21e-08, -1.21e-08, -1.21e-08,
       -1.21e-08, -1.21e-08])
...
ADS1299 API test stream stopping
Test Over
```

## 12/01/2021: ADC Biasing Circuit Error and Demo
**Objectives:** Debug the ADC Biasing Circuit and Prepare for Demo 

**Outcome:** In this session, Casey realized that he had fabricated the wrong trace for the biasing circuit of the ADC. With that, Casey was working on cutting the trace and jumping the wires to make sure that the ADC circuit was biased correctly. While Casey was working on that, I was preparing for the demo by making sure our camera module work and organizing how we were going to present and introdcue our project to the professor and TAs. On that day, we demo'ed our project to Professor Schuh, Bonhyun, and Josephine. We showed that each individual module works and the difficulties we went through with our first board. We were able to display that the camera module works, that the Raspberry Pi is able to communicate with the ADC, and that the firmware was able to flash on our first STM32 board. We believe that we are one board iteration away from having a fully functional device with all the subsystems working. Overall, we believe that there are many improvements that could be done and that this project can be continued by another group. 

You can view our final project video here: [EpiCap Final Video](https://drive.google.com/file/d/1y5SwI7esAfmkpL4BW7Ep7nDs3_et-Z3s/view?usp=sharing)
